<?php
/**
 * Supply.php - Temporary AJAX script for checking order supplies.
 *
 * @author Bugslayer
 *
 */

// Process global query vars or set default values
$article = 'none';
if (isset ( $_GET ['article'] )) {
	$article = $_GET ['article'];
}

$value = 0;
if (isset ( $_GET ['value'] )) {
	$value = $_GET ['value'];
}


// Determine and process the action.
switch ($article) {
	case "slevels" :
		echo check_slevels($value);
		break;
	case "wigbekken" :
		echo check_wigbekken($value);
		break;
	case "zwenkmoeren" :
		echo check_zwenkmoeren($value);
		break;
	default :
		die ( "Illegal action" );
}

function check_slevels($value) {
	// TODO Hard coded supply, for testing purposes only
	return $value <= 100;
}

function check_wigbekken($value) {
	// TODO Hard coded supply, for testing purposes only
	return $value <= 33;
}

function check_zwenkmoeren($value) {
	// TODO Hard coded supply, for testing purposes only
	return $value <= 4;
}

?>