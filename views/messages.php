<?php 
/**
 * Messages.php - renders, well, it should render a list of messages
 * 
 * @author Bugslayer
 * 
 */
 
// Check if the request is done by an authorized user. If not, show 401.php and exit
if (!isAuthenticated()) {
	include '401.php';
	exit();
}
?>
<h1>Berichten</h1>
<div class="panel panel-default">
  <div class="panel-heading">
  	<strong>[Hier de subject van elk bericht]</strong>
  </div>
  <div class="panel-body">
    [Hier de Message van elk bericht]
  </div>
  <div class="panel-footer">
  	<small>[Hier de naam van de afzender (Name-first, Name-middle en Name-last netjes achter elkaar)]</small>
  </div>
</div>
