<?php
/**
 * Login.php - renders a login form
 * 
 * @author Bugslayer
 * 
 */
?>
<form class="form-horizontal" method="post"
	action="index.php?action=login&page=login">
	<div class="form-group">
		<label class="control-label col-sm-offset-2 col-sm-2" for="userid">Gebruikersnaam</label>
		<div class="col-sm-3">
			<input class="form-control" type="text" name="userid" maxlength="50"
				size="30">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-offset-2 col-sm-2" for="password">Wachtwoord</label>
		<div class="col-sm-3">
			<input class="form-control" type="password" name="password" value=""
				maxlength="50" size="30">
		</div>

	</div>
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-3">
			<button type="submit" name="commit"  class="btn btn-default">Login</button>
		</div>
	</div>
</form>

<!-- html voor @@project -->
